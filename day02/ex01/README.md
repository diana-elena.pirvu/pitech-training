List comprehension offers a simplified syntax to create a list based on an existing list or range.
newlist = [expression for item in iterable if condition == True]
