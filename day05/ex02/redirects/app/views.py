from flask import render_template
from flask.helpers import url_for
from werkzeug.utils import redirect
from app import app

@app.route("/")
def index():
    return redirect(url_for("homepage"))

@app.route("/homepage",  methods=['GET'])
def homepage():
    return render_template("/public/homepage.html")

@app.route("/contact",  methods=['GET'])
def contact():
    contact_details = {"Nume":"Hotel Pitech", "Adresa":"str. Memorandumului, nr. 21, Cluj-Napoca, Cluj", "Telefon":"0773 ... ...", "E-mail":"cazare@pitech.ro"}
    return render_template("/public/contact.html", contact_details = contact_details)

@app.route("/prices", methods=['GET'])
def prices():
    prices = [{"Serviciu":"Cameră simplă", "Preț":100}, {"Serviciu":"Cameră dubla", "Preț":200}, {"Serviciu":"Mic dejun", "Preț":50}]
    #return render_template("/public/prices.html")
    return render_template("/public/prices.html", prices=prices)