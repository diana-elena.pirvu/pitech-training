from flask import render_template
from app import app
@app.route("/hello_to_training/<name>")
def index(name):
    return render_template("/public/index.html", name=name)