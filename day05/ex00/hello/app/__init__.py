from flask import Flask, render_template

app = Flask(__name__, template_folder='templates')

@app.route("/")
@app.route("/hello_to_training")
def index():
    return render_template("/public/index.html")
