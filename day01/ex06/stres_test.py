import random

if __name__ == "__main__":
    for i in range(10):
        nr = random.randint(1,10)
        if nr >= 4 and nr<= 6:
            raise ValueError("Number between 4 and 6")
        else:
            print(nr)
