#!/bin/bash

#check if virtualenv is installed
pip3 show virtualenv

#install virtualenv if it is not installed,
#otherwise create my-env and activate it
if [ "$$" == "" ]; then
	pip3 install virtualenv
else
	virtualenv my-env
	source ./my-env/bin/activate
fi
