def create_types():
    list_containter = []
    tuple_container = ()
    dictionary_container = {}
    message = "Invalid input"
    f = None
    try:
        f = open("./demo.txt")
    except:
        raise FileNotFoundError(message)

    lines =  f.readlines()  
    if len(lines)==0 or len(lines)>1:
        raise EOFError(message)
    else:
        index = 0
        contents = lines[0].split(",")
        for i in contents:
            try:
                number = int(i)
                list_containter.append(number)
                dictionary_container[index] = number
                index = index+1
                tuple_container = tuple(list_containter)
            except:
                raise TypeError(message)
    return list_containter, tuple_container, dictionary_container
        
