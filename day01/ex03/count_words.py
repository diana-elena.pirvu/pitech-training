def get_count():
    sentence = input()
    words = sentence.split(" ")
    if len(words) == 0 or (len(words)==1 and words[0]==""):
        raise EOFError( 'No input provided')
    else:
        print(len(words), " words in the sentence")
