Compute Engine is a computing and hosting service that lets you create and run virtual machines on Google infrastructure.
Compute Engine offers scale, performance, and value that lets you easily launch large compute clusters on Google's infrastructure. There are no upfront investments, and you can run thousands of virtual CPUs on a system that offers quick, consistent performance.
