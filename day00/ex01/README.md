Used the command CHMOD to change the permissions(which users are allowed to read, write and execute a file).
The first argument of the command is an octal number.
Each digit changes the permission for owner, group and other users respectively.
4 - permission to read
2- permission to write
1- permission to execute
4+2 = 6 - permission to read and write
4+1 = 5 - permission to read and execute
4+2+1=7 - permission to read, write and execute
